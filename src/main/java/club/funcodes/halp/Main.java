// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.halp;

import static org.refcodes.cli.CliSugar.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileAlreadyExistsException;

import org.refcodes.archetype.CliHelper;
import org.refcodes.cli.Example;
import org.refcodes.cli.Flag;
import org.refcodes.cli.IntOption;
import org.refcodes.cli.LongOption;
import org.refcodes.cli.StringOption;
import org.refcodes.cli.Term;
import org.refcodes.data.AsciiColorPalette;
import org.refcodes.exception.BugException;
import org.refcodes.exception.Trap;
import org.refcodes.hal.HalClient;
import org.refcodes.hal.HalData;
import org.refcodes.hal.HalDataPage;
import org.refcodes.hal.HalStruct;
import org.refcodes.logger.RuntimeLogger;
import org.refcodes.logger.RuntimeLoggerFactorySingleton;
import org.refcodes.properties.ext.application.ApplicationProperties;
import org.refcodes.rest.OauthTokenHandler;
import org.refcodes.textual.Font;
import org.refcodes.textual.FontFamily;
import org.refcodes.textual.FontStyle;
import org.refcodes.textual.SecretHintBuilder;
import org.refcodes.web.OauthToken;

/**
 * Simple application providing a RESTful interface for the JSHELL REPL ([R]ead,
 * [E]xecute, [P]rint, [L]oop..
 */
public class Main {

	private static final RuntimeLogger LOGGER = RuntimeLoggerFactorySingleton.createRuntimeLogger();

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String NAME = "halp";
	private static final String TITLE = "<<< HalP >>>";
	private static final String DEFAULT_CONFIG = NAME + ".ini";
	private static final String DESCRIPTION = "The(HAL-Processor) artifact provides a command line interface (CLI) for HAL (Hypermedia Application Language) enabled REST resources. Get inspired by [https://bitbucket.org/funcodez].";
	private static final String LICENSE_NOTE = "Licensed under GNU General Public License, v3.0 and Apache License, v2.0";
	private static final String COPYRIGHT = "Copyright (c) by CLUB.FUNCODES (see [https://www.funcodes.club])";
	private static final char[] BANNER_PALETTE = AsciiColorPalette.MAX_LEVEL_GRAY.getPalette();
	private static final Font BANNER_FONT = new Font( FontFamily.DIALOG, FontStyle.BOLD );

	private static final int DEFAULT_PAGE_SIZE = 20;
	private static final String TID = "id";
	private static final String URL = "url";
	private static final String ENTITIES = "entities";
	private static final String ALL = "all";
	private static final String CONFIGURE = "configure";
	private static final String INTROSPECT = "introspect";
	private static final String ENTITY = "entity";
	private static final String SIZE = "size";
	private static final String PAGE = "page";

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	public Main( String[] args ) {

		// ---------------------------------------------------------------------
		// CLI:
		// ---------------------------------------------------------------------

		final StringOption theHalUrlArg = stringOption( 'u', "url", URL, "The URL pointing to the HAL resource to be queried." );
		final StringOption theEntityArg = stringOption( 'e', "entity", ENTITY, "The entity on which to operate." );
		final LongOption theIdArg = longOption( "id", TID, "The TID of the entity on which to operate." );
		final IntOption thePageArg = intOption( 'p', "page", PAGE, "The page of the result set regarding pagination" );
		final IntOption theSizeArg = intOption( 's', "size", SIZE, "The page size of the result set's page regarding pagination" );
		final Flag theEntitiesFlag = flag( "entities", ENTITIES, "All entities are to be considered." );
		final Flag theReadAllFlag = flag( 'a', "all", ALL, "Get all specified data." );
		final Flag theIntrospectFlag = flag( 'i', "introspect", INTROSPECT, "Introspection is to be performed." );
		final Flag theConfigureFlag = flag( 'c', "configure", CONFIGURE, "A sample configuration file is written out." );
		final Flag theSysInfoFlag = sysInfoFlag( false );
		final Flag theHelpFlag = helpFlag();
		final Flag theVerboseFlag = verboseFlag();
		final Flag theDebugFlag = debugFlag();

		// @formatter:off
		final Term theArgsSyntax =  
			xor( 
				and(
					optional(
						theHalUrlArg, theVerboseFlag, theDebugFlag
					),
					xor(
						theEntitiesFlag,
						and( 
							theEntityArg,
							xor( 
								theIdArg, 
								theIntrospectFlag,
								and ( theReadAllFlag, any( and ( thePageArg, any( theSizeArg ) ) ) )
							)
						)
					)
				),
				and(
					xor( theHelpFlag, theSysInfoFlag, and( theConfigureFlag, optional( theDebugFlag, theVerboseFlag ) ) ) 
				)
			);
		final Example[] theExamples = examples(
			example( "To show the help text", theHelpFlag ),
			example( "To print the system info", theSysInfoFlag )
		);
		final CliHelper theCliHelper = CliHelper.builder().
			withArgs( args ).
			// withArgs( args, ArgsFilter.D_XX ).
			withArgsSyntax( theArgsSyntax ).
			withExamples( theExamples ).
			withFilePath( DEFAULT_CONFIG ). // Must be the name of the default (template) configuration file below "/src/main/resources"
			withResourceClass( Main.class ).
			withName( NAME ).
			withTitle( TITLE ).
			withDescription( DESCRIPTION ).
			withLicense( LICENSE_NOTE ).
			withCopyright( COPYRIGHT ).
			withBannerFont( BANNER_FONT ).
			withBannerFontPalette( BANNER_PALETTE ).
			withLogger( LOGGER ).build();
		// @formatter:on

		final ApplicationProperties theArgsProperties = theCliHelper.getApplicationProperties();

		// ---------------------------------------------------------------------
		// MAIN:
		// ---------------------------------------------------------------------

		final boolean isEntities = theArgsProperties.getBoolean( theEntitiesFlag );
		final String theEntity = theArgsProperties.get( theEntityArg );
		final Long theId = theArgsProperties.getLong( theIdArg );
		final boolean isIntrospect = theArgsProperties.getBoolean( theIntrospectFlag );
		final boolean isReadAll = theArgsProperties.getBoolean( theReadAllFlag );
		final boolean isDebug = theArgsProperties.getBoolean( theDebugFlag );
		final boolean isVerbose = theArgsProperties.getBoolean( theVerboseFlag );
		final String theUrl = theArgsProperties.get( theHalUrlArg );
		final String theOAuthUrl = theArgsProperties.get( "oauth/url" );
		final String theOAuthClientId = theArgsProperties.get( "oauth/client/id" );
		final String theOAuthClientSecret = theArgsProperties.get( "oauth/client/secret" );
		final String theOAuthUserName = theArgsProperties.get( "oauth/user/name" );
		final String theOAuthUserPassword = theArgsProperties.get( "oauth/user/password" );

		if ( isVerbose ) {
			LOGGER.info( "OAuth-URL = " + theOAuthUrl );
			LOGGER.info( "OAuth-Client-TID = " + theOAuthClientId );
			LOGGER.info( "OAuth-Client-Secret = \"" + SecretHintBuilder.asString( theOAuthClientSecret ) + "\"" );
			LOGGER.info( "OAuth-User-Name = " + theOAuthUserName );
			LOGGER.info( "OAuth-User-Password = \"" + SecretHintBuilder.asString( theOAuthUserPassword ) + "\"" );
		}

		try {
			final HalClient theClient = new HalClient( theUrl );
			if ( theOAuthUrl != null && theOAuthUrl.length() != 0 ) {
				OauthToken theOautToken = new OauthTokenHandler( theOAuthUrl, theOAuthClientId, theOAuthClientSecret, theOAuthUserName, theOAuthUserPassword );
				theClient.setOauthToken( theOautToken );
			}

			// Entities |-->
			if ( isEntities ) {
				final String[] theEntities = theClient.entities();
				for ( int i = 0; i < theEntities.length; i++ ) {
					if ( isDebug ) {
						LOGGER.info( theEntities[i] );
					}
					else {
						System.out.println( theEntities[i] );
					}
				}
				return;
			}
			// Entities <--|

			// Entity element by TID |-->
			else if ( theEntity != null && theId != null ) {
				final HalData theHalData = theClient.read( theEntity, theId );
				if ( isDebug ) {
					for ( String eKey : theHalData.sortedKeys() ) {
						LOGGER.info( eKey + " = " + theHalData.get( eKey ) );
					}
				}
				else {
					System.out.println( theHalData.toPrintable() );
				}
				return;
			}
			// Entity element by TID <--|

			// Introspect entity |-->
			else if ( theEntity != null && isIntrospect ) {
				final HalStruct theHalStruct = theClient.introspect( theEntity );
				if ( isDebug ) {
					for ( String eKey : theHalStruct.sortedKeys() ) {
						LOGGER.info( eKey + " = " + theHalStruct.get( eKey ) );
					}
				}
				else {
					System.out.println( theHalStruct.toPrintable() );
				}
				return;
			}
			// Introspect entity <--|

			// List entity elements |-->
			else if ( theEntity != null && isReadAll ) {
				final HalDataPage theHalDataPage;
				if ( theArgsProperties.containsKey( thePageArg ) ) {
					int thePage = theArgsProperties.getInt( thePageArg );
					int theSize = DEFAULT_PAGE_SIZE;
					if ( theArgsProperties.containsKey( theSizeArg ) ) {
						theSize = theArgsProperties.getInt( theSizeArg );
					}
					theHalDataPage = theClient.readPage( theEntity, thePage, theSize );
					if ( isDebug ) {
						LOGGER.info( "Retrieved page <" + theHalDataPage.getPageNumber() + "> of a total of <" + theHalDataPage.getPageCount() + "> pages with a page size of <" + theHalDataPage.getPageSize() + ">." );
					}
				}
				else {
					theHalDataPage = theClient.readAll( theEntity );
				}
				for ( HalData eHalData : theHalDataPage ) {
					if ( isDebug ) {
						for ( String eKey : eHalData.sortedKeys() ) {
							LOGGER.info( eKey + " = " + eHalData.get( eKey ) );
						}
					}
					else {
						System.out.println( eHalData.toPrintable() );
					}
				}
				return;
			}
			// List entity elements <--|

			throw new BugException( "We never should end up here!" );
		}
		catch ( Exception e ) {
			if ( isDebug ) {
				if ( isVerbose ) {
					System.err.println( Trap.asMessage( e ) );
				}
				else {
					LOGGER.error( Trap.asMessage( e ), e );
				}
			}
			System.exit( e.hashCode() % 0xFF );
		}
	}

	public static void main( String args[] ) {
		new Main( args );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HOOKS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Writes out a sample configuration file located in the root of your
	 * resources folder.
	 * 
	 * @param aConfigFileName The file name of the configuration file found in
	 *        your resources folder.
	 * 
	 * @throws FileAlreadyExistsException Thrown in case an according
	 *         configuration file already exists.
	 * @throws IOException Thrown in case there were problems writing out the
	 *         file.
	 */
	protected static File writeConfigFile( String aConfigFileName ) throws IOException {
		final File theConfigFile = new File( org.refcodes.runtime.Execution.toLauncherDir(), aConfigFileName );
		if ( theConfigFile.exists() ) {
			throw new FileAlreadyExistsException( theConfigFile.getAbsolutePath(), theConfigFile.getAbsolutePath(), "A file with the same name already exists in the targeted path!" );
		}
		final InputStream theInStream = Main.class.getResourceAsStream( "/" + aConfigFileName );
		try ( OutputStream theOutStream = new FileOutputStream( theConfigFile ) ) {
			byte[] theBuffer = new byte[theInStream.available()];
			theInStream.read( theBuffer );
			theOutStream.write( theBuffer );
			theOutStream.flush();
		}
		return theConfigFile;
	}
}
