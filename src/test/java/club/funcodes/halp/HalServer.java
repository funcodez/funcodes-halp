// /////////////////////////////////////////////////////////////////////////////
// FUNCODES.CLUB
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package club.funcodes.halp;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Simple launcher for the HAL server.
 */
@SpringBootApplication
@Configuration
@EnableJpaRepositories(basePackages = "club.funcodes.halp")
public class HalServer {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final int HAL_PORT = 8080;

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ConfigurableApplicationContext _ctx;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	public HalServer() {}

	public HalServer( String[] args ) {
		start( args );
	}

	public HalServer( int aPort ) {
		// System.setProperty( "server.port", "" + aPort );
		// System.setProperty( "eureka.client.serviceUrl.defaultZone",
		// "http://localhost:" + aPort + "/eureka" );
		String[] args = new String[] { "--server.port=" + aPort };
		start( args );
	}

	// /////////////////////////////////////////////////////////////////////////
	// MAIN:
	// /////////////////////////////////////////////////////////////////////////

	public static void main( String[] args ) {
		HalServer theHalServer = new HalServer( HAL_PORT );
		createTestFixures( theHalServer );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	public void start( String[] args ) {
		_ctx = SpringApplication.run( HalServer.class, args );
	}

	public LibraryRepository getLibraryRepository() {
		return _ctx.getBean( LibraryRepository.class );
	}

	public BookRepository getBookRepository() {
		return _ctx.getBean( BookRepository.class );
	}

	public AuthorRepository getAuthorRepository() {
		return _ctx.getBean( AuthorRepository.class );
	}

	public AddressRepository getAddressRepository() {
		return _ctx.getBean( AddressRepository.class );
	}

	public PersonRepository getPersonRepository() {
		return _ctx.getBean( PersonRepository.class );
	}

	public UserRepository getUserRepository() {
		return _ctx.getBean( UserRepository.class );
	}

	public void destroy() {
		SpringApplication.exit( _ctx );
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	protected String toNormalizedValue( Integer aHalPort, String aValue ) {
		if ( aValue != null ) {
			aValue = aValue.replaceAll( Pattern.quote( aHalPort.toString() ), "PORT" );
		}
		return aValue;
	}

	protected static void createTestFixures( HalServer aHalServer ) {
		Address addressA = new Address( "Location_A" );
		Address addressB = new Address( "Location_B" );
		addressA = aHalServer.getAddressRepository().save( addressA );
		addressB = aHalServer.getAddressRepository().save( addressB );
		Library libraryA = new Library( "Library_A" );
		Library libraryB = new Library( "Library_B" );
		libraryA.setAddress( addressA );
		libraryB.setAddress( addressB );
		libraryA = aHalServer.getLibraryRepository().save( libraryA );
		libraryB = aHalServer.getLibraryRepository().save( libraryB );

		Book bookA1 = new Book( "Book_A1" );
		Book bookB1 = new Book( "Book_B1" );

		List<Book> booksA = new ArrayList<>( Arrays.asList( bookA1 ) );
		List<Book> booksB = new ArrayList<>( Arrays.asList( bookB1 ) );
		bookA1 = aHalServer.getBookRepository().save( bookA1 );
		bookB1 = aHalServer.getBookRepository().save( bookB1 );

		Author authorA1 = new Author( "authorA1", booksA );
		Author authorA2 = new Author( "authorA2", booksA );
		Author authorB1 = new Author( "authorB1", booksB );
		Author authorB2 = new Author( "authorB2", booksB );
		authorA1 = aHalServer.getAuthorRepository().save( authorA1 );
		authorA2 = aHalServer.getAuthorRepository().save( authorA2 );
		authorB1 = aHalServer.getAuthorRepository().save( authorB1 );
		authorB2 = aHalServer.getAuthorRepository().save( authorB2 );
		List<Author> authorsA = new ArrayList<>( Arrays.asList( authorA1, authorA2 ) );
		List<Author> authorsB = new ArrayList<>( Arrays.asList( authorB1, authorB2 ) );

		bookA1.setAuthors( authorsA );
		bookB1.setAuthors( authorsB );
		bookA1.setLibrary( libraryA );
		bookB1.setLibrary( libraryB );
		bookA1 = aHalServer.getBookRepository().save( bookA1 );
		bookB1 = aHalServer.getBookRepository().save( bookB1 );
		Location locationA = new Location( "addressLine1_A", "addressLine2_A", "city_A", "state_A", "country_A", "zipCode_A" );
		Location locationB = new Location( "addressLine1_B", "addressLine2_B", "city_B", "state_B", "country_B", "zipCode_B" );
		Name nameA = new Name( "firstName_A", "middleName_A", "lastName_A" );
		Name nameB = new Name( "firstName_B", "middleName_B", "lastName_B" );
		Person personA = new Person( nameA, "personA@aaa.com", locationA );
		Person personB = new Person( nameB, "personB@bbb.com", locationB );
		personA = aHalServer.getPersonRepository().save( personA );
		personB = aHalServer.getPersonRepository().save( personB );
		User userA = new User( "userA", "userA@aaa.com" );
		User userB = new User( "userB", "userB@bbb.com" );
		userA = aHalServer.getUserRepository().save( userA );
		userB = aHalServer.getUserRepository().save( userB );
	}

	protected Person createPerson( String aSuffix ) {
		Location location = new Location( "addressLine1_" + aSuffix, "addressLine2_" + aSuffix, "city_" + aSuffix, "state_" + aSuffix, "country_" + aSuffix, "zipCode_" + aSuffix );
		Name name = new Name( "firstName_" + aSuffix, "middleName_" + aSuffix, "lastName_" + aSuffix );
		Person person = new Person( name, "person" + aSuffix + "@aaa.com", location );
		return person;
	}

	// /////////////////////////////////////////////////////////////////////////
	// DAEMONS:
	// /////////////////////////////////////////////////////////////////////////

}
