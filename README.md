# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***[`HALP`](https://bitbucket.org/funcodez/funcodes-halp) stands for [`HAL`](https://en.wikipedia.org/wiki/Hypertext_Application_Language) Explorer and represents a command line tool for exploring [Hyper Media Application Language](https://tools.ietf.org/html/draft-kelly-json-hal-00) ([`HAL`](https://tools.ietf.org/html/draft-kelly-json-hal-00)) enabled [REST](https://en.wikipedia.org/wiki/Representational_state_transfer) resources. [`HALP`](https://bitbucket.org/funcodez/funcodes-halp) harnesses the [`refcodes-hal`](https://bitbucket.org/refcodes/refcodes-hal) library and therewith demonstrates its usage for your own programs.***

## Getting started ##

To get up and running, clone the [`funcodes-halp`](https://bitbucket.org/funcodez/funcodes-halp/) repository from [`bitbucket`](https://bitbucket.org/funcodez/funcodes-halp)'s `git` repository. 

## How do I get set up? ##

Using `SSH`, go as follows to get the [`Maven`](https://en.wikipedia.org/wiki/Apache_Maven) [`HALP`](https://bitbucket.org/funcodez/funcodes-halp/) project:

```
git clone git@bitbucket.org:funcodez/funcodes-halp.git
```

Using `HTTPS`, go accordingly as follows to get the [`Maven`](https://en.wikipedia.org/wiki/Apache_Maven) [`halp`](https://bitbucket.org/funcodez/funcodes-halp/) project:

```
git clone https://bitbucket.org/funcodez/funcodes-halp.git
```

Then you can build a [`fat-jar`](https://maven.apache.org/plugins/maven-shade-plugin/examples/executable-jar.html) and launch the application: 

```
cd funcodes-halp
mvn clean install
java -jar target/funcodes-halp-0.0.1.jar
```

## Big fat executable bash script (optional) ##

This step is optional, though when running your application under `Linux`, the following will be your friend:

> To build a big fat single executable [`bash`](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) script, take a look at the [`scriptify.sh`](https://bitbucket.org/funcodez/funcodes-halp/src/master/scriptify.sh) script and the [`build.sh`](https://bitbucket.org/funcodez/funcodes-halp/src/master/build.sh) script respectively:

```
./scriptify.sh
./target/halp-launcher-x.y.z.sh
```
The resulting `halp-launcher-x.y.z.sh` file is a big fat single executable [`bash`](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) script being launched via `./target/halp-launcher-x.y.z.sh`.

> Building and creating an executable bash script is done by calling `./build.sh`!

## First steps ##

Go for `./target/halp-launcher-x.y.z.sh --help` (or `java -jar target/funcodes-halp-0.0.1.jar` if you wish) to get instructions on how to invoke the tool.

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/funcodez/funcodes-halp/issues)
* Add a nifty user-interface
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.
